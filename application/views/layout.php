<?php
$this->load->view('includes/header');  //site header
if(isset($inner_menu))
	$this->load->view($inner_menu);  //Inner Menu
if(isset($left_menu))
	$this->load->view($left_menu);  //Left Menu
$this->load->view($main_content);  //comes from controller. 
$this->load->view('includes/footer');  //site footer 
?>