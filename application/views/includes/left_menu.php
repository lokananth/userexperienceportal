<aside id="left-panel">
	<!-- User info -->
	<div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 			
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
			<!--<i class="fa fa-lg fa-fw fa-user"></i>
				<img src="img/avatars/sunny.png" alt="me" class="online" /> -->
				<span></span> 
			</a> 			
		</span>
	</div>
	<!-- end user info -->

	<!-- NAVIGATION : This navigation is also responsive-->
	<nav>
		<ul>
			<li class="active">
				<a href="index.php" title="Dashboard"><i class="fa fa-lg fa-fw fa-phone-square"></i> <span class="menu-item-parent">Call, SMS & Data</span></a>
			</li>			
			<li>
				<a href="bundle.php" title="Dashboard"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Bundle</span></a>
			</li>	
			  
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> 
		<i class="fa fa-arrow-circle-left hit"></i> 
	</span>

</aside>
<!-- END NAVIGATION -->