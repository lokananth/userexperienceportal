<?php
/**
 * Dashboard
 *
 * 
 * @package		CodeIgniter
 * @subpackage	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Dashboard Controllers
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * @link		
 */
class Dashboard extends CI_Controller {


	public function index()
	{
		if ($this->input->post()) {
			redirect('home');
		}
		$data['pagetitle']  = $this->config->item('getproject_name') . ' - Dashboard';
		$data['inner_menu'] = 'includes/inner_menu';
		$data['left_menu']  = 'includes/left_menu';
		$data['main_content'] = 'dashboard/dashboard';
		$this->load->view('layout', $data);		
	}
}
