<?php
/**
 * Home
 *
 * 
 * @package		CodeIgniter
 * @subpackage	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Home Controllers
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * @link		
 */
class Home extends CI_Controller {


	public function index()
	{
		if ($this->input->post()) {
			redirect('home');
		}
		$data['pagetitle']  = $this->config->item('getproject_name') . ' - Home page';
		$data['inner_menu'] = 'includes/inner_menu';
		$data['left_menu']  = 'includes/left_menu';
		$data['main_content'] = 'home/dashboard';
		$this->load->view('layout', $data);		
	}
}
