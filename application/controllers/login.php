<?php
/**
 * Login
 *
 * 
 * @package		CodeIgniter
 * @subpackage	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Login Controllers
 *
 * @package		CodeIgniter
 * @subpackage	Controller
 * @category	Controller
 * @author		Ramachandran Pandian - Mundio Dev Team <p.ramachandran@mundio.com>
 * @link		
 */
class Login extends CI_Controller {


	public function index()
	{
		if ($this->input->post()) {
			redirect('home');
		}
		$data['pagetitle'] = $this->config->item('getproject_name') . ' - Login';
		$data['main_content'] = 'login/login';
		$this->load->view('layout', $data);		
	}
}
